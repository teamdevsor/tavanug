<?php

namespace App\Http\Controllers;

use App\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    public $user;

    public function today(Request $request)
    {
        // TODO middleware
        $init = $this->init($request);

        if ($init['status'] == false) {
            return $init;
        }

        $words = $this->user->todaywords()->get();


        if (sizeof($words) == 0) {

            $level = $this->user->profile->level;
            $word_count = $this->user->profile->word_count;
            $language_id = $this->user->profile->language_id;
            $user_word_id_array = $this->user->getWordIdArray();

            $words = Word::where('level', '<=', $level)
                ->where('language_id', $language_id)
                ->whereNotIn('id', $user_word_id_array)
                ->inRandomOrder()
                ->get()
                ->take($word_count);

            $save = [];
            foreach ($words as $word) {
                $this->user->words()->save($word,['state'=>1]);
                // $save[$word->id] = ['state'=>1];
            }

        }

        $return = [];
        foreach ($words as $k => $word) {
            $return[$k] = $word;
            $return[$k]['translations'] = $word->translations;
        }

        if (!empty($words)) {
            return [
                'status' => true,
                'words' => $return,
            ];
        } else {
            return [
                'status' => false,
                'message' => 'No Words Left',
            ];
        }
    }
}
