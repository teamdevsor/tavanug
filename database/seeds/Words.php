<?php

use App\Language;
use App\Translation;
use App\Word;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class Words extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            'mn' => "Монгол",
            'en' => "English",
        ];

        Language::query()->truncate();
        Word::query()->truncate();
        Translation::query()->truncate();

        foreach ($languages as $code => $title) {
            DB::table('languages')->insert([
                'title' => $title,
                'code' => $code,
            ]);
        }

        $words = [];
        $faker = Faker::create();
        for ($i = 0; $i < 99; $i++) {
            $words[] = $faker->word;
        }
        foreach ($words as $w) {
            $word = new Word;
            $word->word = $w;
            $word->language_id = 1;
            $word->level = rand(1, 10);
            $word->save();

            $translations = [];
            $rand = rand(1, 4);
            for ($i = 0; $i < $rand; $i++) {
                $translations[] = $faker->word;
            }

            foreach ($translations as $ww) {
                $t = new Translation;
                $t->word = $ww;
                $t->language_id = 2;
                $t->word_id = $word->id;
                $t->save();
            }
        }
    }
}
