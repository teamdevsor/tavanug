<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTranslationsTable extends Migration {

	public function up()
	{
		Schema::create('translations', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('word_id');
			$table->integer('language_id');
			$table->string('word');
		});
	}

	public function down()
	{
		Schema::drop('translations');
	}
}