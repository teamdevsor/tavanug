<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWordTable extends Migration {

	public function up()
	{
		Schema::create('user_word', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id');
			$table->string('word_id');
			$table->smallInteger('state');
		});
	}

	public function down()
	{
		Schema::drop('user_word');
	}
}